import { Component, ViewChild, OnInit } from '@angular/core';
import { App, NavController, Content, PopoverController, Loading, LoadingController } from 'ionic-angular';
import { PostPopover } from './post-popover';
import { Messages } from '../messages/messages';

import { FeedHandsProvider } from '../../providers/feed-hands/feed-hands';
import { FeedHand } from "../../models/feedHand";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [FeedHandsProvider]
})
export class Home implements OnInit {


  ngOnInit(): void {
    let loading = this.createAndShowLoading(null);
    this.loadFeedhands()
      .then(() => loading.dismiss());
  }

  @ViewChild(Content) content: Content;

  public like_btn = {
    color: 'black',
    icon_name: 'heart-outline'
  };

  public tap: number = 0;
  public feedHands = [];

  constructor(public navCtrl: NavController, public popoverCtrl: PopoverController, public app: App, private feedHandProvider: FeedHandsProvider,
    public loadingCtrl: LoadingController) {
  }

  likeButton() {
    if (this.like_btn.icon_name === 'heart-outline') {
      this.like_btn.icon_name = 'heart';
      this.like_btn.color = 'danger';
      // Do some API job in here for real!
    }
    else {
      this.like_btn.icon_name = 'heart-outline';
      this.like_btn.color = 'black';
    }
  }

  tapPhotoLike(times) { // If we click double times, it will trigger like the post
    this.tap++;
    if (this.tap % 2 === 0) {
      this.likeButton();
    }
  }

  presentPostPopover() {
    let popover = this.popoverCtrl.create(PostPopover);
    popover.present();
  }

  goMessages() {
    this.app.getRootNav().push(Messages);
  }

  swipePage(event) {
    if (event.direction === 1) { // Swipe Left
      console.log("Swap Camera");
    }

    if (event.direction === 2) { // Swipe Right
      this.goMessages();
    }

  }

  scrollToTop() {
    this.content.scrollToTop();
  }

  private loadFeedhands(): Promise<Array<FeedHand>> {
    return this.feedHandProvider.getFeed()
      .then(feedHandList => this.feedHands = feedHandList);
  }

  private createAndShowLoading(messageContent: string): Loading {
    let options = {
      content: messageContent || "Aguarde, carregando..."
    };
    let loader = this.loadingCtrl.create(options);
    loader.present();
    return loader;
  }

}
