import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';

@Component({
    selector: 'page-login',
    templateUrl: 'login.html'
})
export class LoginPage implements OnInit {

    constructor(public navCtrl: NavController) {
    }

    ngOnInit(): void {
    }

    public doLogin(): void {
        this.navCtrl.push(TabsPage);
    }

}
