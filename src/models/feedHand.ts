
export interface FeedHand {
    id: Number;
    description: String;
    rating: Number;
    geomaps: Number;
    numberHands: Number;
    date_create: Date;
}