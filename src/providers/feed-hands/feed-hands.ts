import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { FeedHand } from "../../models/feedHand";

/*
  Generated class for the FeedHandsProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class FeedHandsProvider {

  constructor(public http: Http) {
  }

  public getFeed(): Promise<Array<FeedHand>> {
    return new Promise((resolver, reject) => {
      this.http.get('assets/data/data.json')
        .subscribe(
        response => {
          setTimeout(function () {
            resolver(response.json());
          }, 2000);
        },
        error => reject(error)
        );
    });
  }

}
